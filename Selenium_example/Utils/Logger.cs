﻿using NLog;
using NLog.Config;
using NLog.Targets;

namespace Utils
{
    class GetLogger
    {
        public static Logger LoggerInstance { get; private set; }

        static GetLogger()
        {
            var config = new LoggingConfiguration();
            var consoleTarget = new ColoredConsoleTarget("consoletarget")
            {
                Layout = @"${date:format=HH\:mm\:ss} ${level} ${message} ${exception}"
            };
            config.AddTarget(consoleTarget);

            var fileTarget = new FileTarget("filetarget")
            {
                FileName = System.Environment.CurrentDirectory + "\\output\\logs\\log.txt",
                Layout = "${longdate} ${level} ${message}  ${exception}"
            };
            config.AddTarget(fileTarget);
            config.AddRuleForAllLevels(fileTarget); 
            config.AddRuleForAllLevels(consoleTarget); 
             
            LogManager.Configuration = config;
            LoggerInstance = LogManager.GetCurrentClassLogger();
        }
    }
}
