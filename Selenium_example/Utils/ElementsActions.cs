﻿using System;
using System.Collections.ObjectModel;
using Utils;
using OpenQA.Selenium;

namespace Utils
{
    public class ElementsActions
    {
        WaitUtil waitUtil;
        public ElementsActions(WaitUtil waitUtil)
        {
            this.waitUtil = waitUtil;
        }

        IWebElement FindElement(IWebDriver driver, By locator)
        {
            try
            {
                var element = driver.FindElement(locator);
                GetLogger.LoggerInstance.Info("Element \"{0}\" was found.", locator.ToString());
                return element;
            }
            catch (Exception e)
            {
               GetLogger.LoggerInstance.Error("Element \"{0}\" was not found. \nException: \n{1}", locator.ToString(),
                   e.Message);
               ScreenshotMaker.MakeScreenshot(driver);
               throw;
            }
        }

        IWebElement WaitFindElement(IWebDriver driver, By locator)
        {
            try
            {
                waitUtil.WaitTillElemntClickable(driver, locator);
                var element = driver.FindElement(locator);
                GetLogger.LoggerInstance.Info("Element \"{0}\" was found.", locator.ToString());
                return element;
            }
            catch (Exception e)
            {
                GetLogger.LoggerInstance.Error("Element \"{0}\" was not found. \nException: \n{1}", locator.ToString(),
                    e.Message);
                ScreenshotMaker.MakeScreenshot(driver);
                throw;
            }
        }

        ReadOnlyCollection<IWebElement> FindElements(IWebDriver driver, By locator)
        {
            var elements = driver.FindElements(locator);
            if (elements.Count>0)
            {
                GetLogger.LoggerInstance.Info("Elements \"{0}\" was found.", locator.ToString());
                return elements;
            }
            else
            {
                var exception = new Exception("Any element {locator.ToString()} was not found.");
                GetLogger.LoggerInstance.Error(exception.Message);
                ScreenshotMaker.MakeScreenshot(driver);
                throw exception;
            }
        }

        ReadOnlyCollection<IWebElement> WaitFindElements(IWebDriver driver, By locator)
        {

            waitUtil.WaitTillElemntClickable(driver, locator);
            var elements = driver.FindElements(locator);
            if (elements.Count > 0)
            {
                GetLogger.LoggerInstance.Info("Element \"{0}\" was found.", locator.ToString());
                return elements;
            }
            else
            {
                var exception = new Exception("Any element {locator.ToString()} was not found.");
                GetLogger.LoggerInstance.Error(exception.Message);
                ScreenshotMaker.MakeScreenshot(driver);
                throw exception;
            }
        }

        public void Clickelement(IWebDriver driver, By locator)
        {
            try
            {
                FindElement(driver, locator).Click();
                GetLogger.LoggerInstance.Info("Element \"{0}\" was clicked.", locator.ToString());
                FindElement(driver, locator).Click();
            }
            catch (Exception e)
            { 
                GetLogger.LoggerInstance.Error("Element \"{0}\" was not clicked is \"{1}\".", locator.ToString(), e.Message);
                ScreenshotMaker.MakeScreenshot(driver);
                throw;
            }
        }

        public void WaitClickelement(IWebDriver driver, By locator)
        {
            try
            {
                waitUtil.WaitTillElemntClickable(driver, locator);
                FindElement(driver, locator).Click();
                GetLogger.LoggerInstance.Info("Element \"{0}\" was clicked.", locator.ToString());

            }
            catch (Exception e)
            {
                GetLogger.LoggerInstance.Error("Element \"{0}\" was not clicked is \"{1}\".", locator.ToString(), e.Message);
                ScreenshotMaker.MakeScreenshot(driver);
                throw;
            }

        }

        public void SendText(IWebDriver driver, By locator, string text)
        {
            var element = FindElement(driver,  locator);
            element.Clear();
            element.SendKeys(text);
        }

        public void WaitSendText(IWebDriver driver, By locator, string text)
        {
            var element = WaitFindElement(driver, locator);
            element.Clear();
            element.SendKeys(text);
        }

        public bool IsElementDisplayed(IWebDriver driver, By locator)
        {
            return FindElement(driver, locator).Displayed;
        }

        public bool WaitIsElementDisplayed(IWebDriver driver, By locator)
        {
            return WaitFindElement(driver, locator).Displayed;
        }

        public string GetText(IWebDriver driver, By locator)
        {
            try
            {
                var text = FindElement(driver, locator).Text;
                GetLogger.LoggerInstance.Info("Element \"{0}\" text is \"{1}\".", locator.ToString(), text);
                return text;
            }
            catch (Exception e)
            {
                GetLogger.LoggerInstance.Error
                ("Not possible to get text for element \"{0}\". \nException: \n{1}", locator.ToString(),
                    e.Message);
                ScreenshotMaker.MakeScreenshot(driver);
                throw;
            }      
        }

        public string WaitGetText(IWebDriver driver, By locator)
        {
            try
            {
                var text = WaitFindElement(driver, locator).Text;
                GetLogger.LoggerInstance.Info("Element \"{0}\" text is \"{1}\".", locator.ToString(), text);
                return text;
            }
            catch (Exception e)
            {
                GetLogger.LoggerInstance.Error
                ("Not possible to get text for element \"{0}\". \nException: \n{1}", locator.ToString(),
                    e.Message);
                ScreenshotMaker.MakeScreenshot(driver);
                throw;
            }
        }
    }
}

