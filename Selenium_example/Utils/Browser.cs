﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Support.Events;
using System;
using System.Collections.Generic;


namespace Utils
{
    public enum Browsers
    {
        IE,
        Firefox,
        Chrome
    }

    public class Browser
    {
        IWebDriver _webDriver;
        EventFiringWebDriver _driver;
        string driverpath = Environment.CurrentDirectory + "\\Drivers";
        Browsers defaultBrowser = Browsers.Chrome;

        public EventFiringWebDriver WebDriver
        {
            get
            {
                if (_webDriver == null)
                    return Start(defaultBrowser);
                else
                {
                    Stop();
                    return Start(defaultBrowser);
                }
            }
        }

        EventFiringWebDriver Start(Browsers browser)
        {
            switch (browser)
            {
                case Browsers.IE:
                    {
                        _webDriver = StartIE();
                        break;
                    }
                case Browsers.Firefox:
                    {
                        _webDriver = StartFirefox();
                        break;
                    }

                case Browsers.Chrome:
                    {
                        _webDriver = StartChrome();
                        break;
                    }
                default:
                    throw new Exception("Failed to select borwser");
            }
            _driver = new EventFiringWebDriver(_webDriver);
            _driver.Manage().Window.Maximize();
            return _driver;
        }

        private  IWebDriver StartChrome()
        {
            try
            {
                var options = new ChromeOptions
                {

                };

                var chrome = new ChromeDriver(driverpath, options);
                 return chrome;
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to start Chrome: "+ ex);
            }
        }

        private  IWebDriver StartFirefox()
        {
            try
            {
                var options = new FirefoxOptions {};
                var ff = new FirefoxDriver(driverpath, options);
                return ff;
            }
            catch
            {
                throw new Exception("Unable to start Firefox");
            }
        }

        IWebDriver StartIE()
        {
            try
            {
                var options = new InternetExplorerOptions
                {
                    IntroduceInstabilityByIgnoringProtectedModeSettings = true,
                    EnableNativeEvents = true
                };

                var ie = new InternetExplorerDriver(driverpath, options);
                return ie;
            }
            catch
            {
                throw new Exception("Unable to start IE");
            }
        }

        public void Stop()
        {
            _webDriver.Close();
            _webDriver.Quit();
            _webDriver = null;
        }

        public void RefreshPage()
        {
            WebDriver.Navigate().Refresh();
        }
    }
}
