﻿using Application;
using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;


namespace Utils
{
    class ExceldataProvider
    {
        string filename = Environment.CurrentDirectory + "\\Excel\\Credentials.xlsx";
        public object Dataset { get; private set; }

        public List<Account> GetCredntials()
        {
            var accounts = new List<Account>();
            //https://stackoverflow.com/questions/49215791/vs-code-c-sharp-system-notsupportedexception-no-data-is-available-for-encodin
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            FileStream fs = new FileStream($"{filename}" , FileMode.Open, FileAccess.Read);
            IExcelDataReader excelReader = ExcelReaderFactory.CreateReader(fs);
            DataSet result = excelReader.AsDataSet();

            while(excelReader.Read())
            {
                accounts.Add(new Account
                {
                    Login = excelReader.GetString(0),
                    Email = excelReader.GetString(1),
                    Password = excelReader.GetString(2)
                });
            }
            return accounts;
        }
    }
}
