﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Utils
{
    public class WaitUtil
    {
        private int waitDefault = 10;

        public void Sleep(int miliseconds)
        {
            Thread.Sleep(miliseconds);
        }

        public void WaitTillElemntClickable(IWebDriver driver, By locator)
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(waitDefault))
                .Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(locator));
        }

        public void WaitTillElemntVisible(IWebDriver driver, By locator)
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(waitDefault))
                .Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(locator));
        }

    }
}
