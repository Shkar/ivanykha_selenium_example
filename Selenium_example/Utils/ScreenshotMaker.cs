﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.Extensions;

namespace Utils
{
    public static class ScreenshotMaker
    {
        static string filepath = System.Environment.CurrentDirectory + "\\output\\screenshots\\";

        public static void MakeScreenshot(IWebDriver driver)
        {
            if (!Directory.Exists(filepath))
            {
                Directory.CreateDirectory(filepath);
            }
            var screenshot = driver.TakeScreenshot();
            StringBuilder fn = new StringBuilder(filepath);
            fn.Append(DateTime.Now.ToString("yyyy-dd-M-HH-mm-ss"));
            fn.Append(@".png");
            string filename = fn.ToString();
            screenshot.SaveAsFile(filename, ScreenshotImageFormat.Png);
        }
    }
}
