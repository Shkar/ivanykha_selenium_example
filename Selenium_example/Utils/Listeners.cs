﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.Events;
using OpenQA.Selenium.Support.Extensions;
using System;
using System.Text;

namespace Utils
{
    public class Listeners
    {
        public string filepath = @"E:\screens\";

        public void firingDriver_ElementClicked(object sender, WebElementEventArgs e)
        {
            var screenshot = e.Driver.TakeScreenshot();
            StringBuilder fn = new StringBuilder(filepath);
            fn.Append(DateTime.Now.ToString("yyyy-dd-M-HH-mm-ss"));
            fn.Append(@".png");
            string filename = fn.ToString();
            screenshot.SaveAsFile(filename, ScreenshotImageFormat.Png);
        }

        public void firingDriver_ExceptionThrown(object sender, WebDriverExceptionEventArgs e)
        {
            var screenshot = e.Driver.TakeScreenshot();
            StringBuilder fn = new StringBuilder(filepath);
            fn.Append(DateTime.Now.ToString("yyyy-dd-M-HH-mm-ss"));
            fn.Append(@".png");
            string filename = fn.ToString();
            screenshot.SaveAsFile(filename, ScreenshotImageFormat.Png);
            GetLogger.LoggerInstance.Error(e.ThrownException);
        }
    }
}
