﻿using OpenQA.Selenium;
using Pages;
using System;
using System.Collections.Generic;
using System.Text;
using Utils;

namespace Pages
{
    public class MyAccountPopUp : BasePage
    {
        public MyAccountPopUp(IWebDriver driver, WaitUtil waitUtil, ElementsActions elementsActions)
            : base(driver, waitUtil, elementsActions)
        { }

        By UserName = By.ClassName("js-customer-name");

        public string GetUserName()
        {
            return elementsActions.GetText(driver, UserName);
        }
    }
}
