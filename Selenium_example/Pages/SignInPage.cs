﻿using Utils;
using OpenQA.Selenium;

namespace Pages
{
    public class SignInPage : BasePage
    {
        ExceldataProvider exceldataProvider;

        By loginEmailLocator = By.Id("login-module-email");
        By passwordLocator = By.Id("login-module-input-password");
        By SignInLocator = By.CssSelector(".track-link.btn.btn-primary.btn-block.btn-sm.loginBtn.js-sign-in-action");


        public SignInPage(IWebDriver driver, WaitUtil waitUtil, ElementsActions elementsActions)
        : base(driver, waitUtil, elementsActions)
        {
            exceldataProvider = new ExceldataProvider();
        }

        public SignInPage InputEmail()
        {
            var loginEmail = exceldataProvider.GetCredntials()[0].Email;
            elementsActions.WaitSendText(driver, loginEmailLocator, loginEmail);
            return this;
        }

        public SignInPage InputPassword()
        {
            var password = exceldataProvider.GetCredntials()[0].Password;
            elementsActions.WaitSendText(driver, passwordLocator, password);
            return this;
        }

        public HomePage SignInClick()
        {
            elementsActions.Clickelement(driver, SignInLocator);
            return new HomePage(driver, waitUtil, elementsActions);
        }
    }
}
