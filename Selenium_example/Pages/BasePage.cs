﻿using Utils;
using OpenQA.Selenium;

namespace Pages
{
    public abstract class BasePage
    {
        protected IWebDriver driver;
        protected WaitUtil waitUtil;
        protected ElementsActions elementsActions;

        public BasePage(IWebDriver driver, WaitUtil waitUtil, ElementsActions elementsActions)
        {
            this.driver = driver;
            this.waitUtil = waitUtil;
            this.elementsActions = elementsActions;
        }
    }
}
