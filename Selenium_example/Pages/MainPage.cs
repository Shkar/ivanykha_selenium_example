﻿using Utils;
using OpenQA.Selenium;

namespace Pages
{
    public class MainPage : BasePage
    {
        By SignInButton = By.ClassName("sign-in-txt");
        static string URL = "https://www.rei.com/";
  
        public MainPage(IWebDriver driver, WaitUtil waitUtil, ElementsActions elementsActions)
            : base(driver, waitUtil, elementsActions)
        { }

        public MainPage Open()
        {
            driver.Url = URL;
            return this;
        }

        public SignInPage OpenSignInPage()
        {
            elementsActions.Clickelement(driver, SignInButton);
            return new SignInPage(driver, waitUtil, elementsActions);
        }
    }
}