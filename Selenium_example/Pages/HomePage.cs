﻿using Utils;
using OpenQA.Selenium;

namespace Pages
{
    public class HomePage : BasePage
    {

        By SignIn = By.ClassName("sign-in-txt");
        By MyAccountLocator = By.ClassName("my-account-txt");


        public HomePage(IWebDriver driver, WaitUtil waitUtil, ElementsActions elementsActions)
            : base(driver, waitUtil, elementsActions)
        {}

        public bool CheckIsAtHomePage()
        {
            return elementsActions.WaitIsElementDisplayed(driver, MyAccountLocator);
        }

        public MyAccountPopUp OpenAccountPopUp()
        {
            elementsActions.WaitClickelement(driver, MyAccountLocator);
            return  new MyAccountPopUp(driver, waitUtil, elementsActions);
        }
    }
}