using System;
using NUnit.Framework;

namespace Application
{
    [TestFixture]
    public class Tests : BaseTest
    {
      
        [Test]
        public void Login()
        {
            LogStart(TestContext.CurrentContext.Test.MethodName);
                   var result = app.mainPage
                    .Open()
                    .OpenSignInPage()
                    .InputEmail()
                    .InputPassword()
                    .SignInClick()
                    .CheckIsAtHomePage();
            Assert.True(result) ;
            LogFinish(TestContext.CurrentContext.Test.MethodName);
        }
    }
}

