﻿using Utils;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using System;
using NUnit.Framework;
using NUnit.Framework.Internal.Commands;


namespace Application
{          
    public class BaseTest
    {
        protected EventFiringWebDriver driver;
        protected Browser browser;
        protected WaitUtil waitUtil;
        protected Appliccation app;
        protected ElementsActions elementsActions;
        protected Listeners listeners;
     
        [SetUp]
        public void Init()
        {
            driver = new Browser().WebDriver;
            listeners =new Listeners();
            //driver.ExceptionThrown += new EventHandler<WebDriverExceptionEventArgs>(listeners.firingDriver_ExceptionThrown);
            //driver.ElementClicked += new EventHandler<WebElementEventArgs>(listeners.firingDriver_ElementClicked);
            waitUtil=new WaitUtil();
            elementsActions = new ElementsActions(new WaitUtil());
            app = new Appliccation(driver, waitUtil, elementsActions);
        }

        /*[OneTimeSetUp]
        public void LogStart()
        {
            GetLogger.LoggerInstance.Info("Test \"{0}\" is exceuting", TestContext.CurrentContext.Test.MethodName);
        }*/

        [TearDown]
        public void StopBrowser()
        {
            driver.Close();
            driver.Quit();
            driver = null;
        }

        public void LogStart(string testname)
        {
            GetLogger.LoggerInstance.Info("********** Test \"{0}\" is started **********", testname);
        }

        public void LogFinish(string testname)
        {
            GetLogger.LoggerInstance.Info("********** Test \"{0}\" is completed **********", testname);
        }
    }
}
